/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import java.util.Scanner;

/**
 *
 * @author nacho
 */
public class Vista {
    public int menu(){
        System.out.println("Menú de Selección");
        System.out.println("-----------------");
        System.out.println("1 - Persona");
        System.out.println("2 - Animal");
        System.out.println("0 - Salir");
        System.out.println("");
        return validaOpcion("Seleccione una Opción (0-2): ", 0, 2);
    }
    
    static int validaOpcion(String msg, int min, int max) {
        int op=0;
        boolean salir=false;
        Scanner sc = new Scanner(System.in);
        do {
            System.out.print(msg);
            try {
                op=sc.nextInt();
                if (op>=min && op<=max) {
                    salir=true;
                } else {
                    System.out.println("La opción ha de estar comprendida entre " + min + " y " + max);
                }
            } catch (Exception e) {
                System.out.println("Opción no válida!!");
                sc.next(); // Para vaciar el buffer del scanner
            }
        } while (!salir);
        return op;
    } 
    
}
