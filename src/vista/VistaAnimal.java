/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import java.io.IOException;
import java.util.Scanner;
import modelo.Animal;

/**
 *
 * @author nacho
 */
public class VistaAnimal {
    public Animal leerAnimal() throws IOException {
        Scanner sc = new Scanner(System.in);
        System.out.print("Introduzca el nombre: ");
        String nombre=sc.next();
        System.out.print("Introduzca la edad: ");
        int edad=sc.nextInt();
        return new Animal(nombre, edad);
    }
    
    public void mostrarAnimal(Animal a) {
        System.out.println("Animal{" + "nombre=" + a.getNombre() + ", edad=" + a.getEdad() + '}');
    }
}
