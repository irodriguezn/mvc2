/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import modelo.*;
import vista.*;

/**
 *
 * @author nacho
 */
public class Controlador {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Vista v=new Vista();
        int op=v.menu();
        switch (op) {
            case 1: //Persona
                VistaPersona vp=new VistaPersona();
                try {
                    Persona p=vp.leerPersona();
                    vp.mostrarPersona(p);
                } catch (Exception e) {
                    System.out.println("Error leyendo persona");
                }
                break;
            case 2: //Animal
                VistaAnimal va=new VistaAnimal();
                try {
                    Animal a=va.leerAnimal();
                    va.mostrarAnimal(a);
                } catch (Exception e) {
                    System.out.println("Error leyendo animal");
                }
                break;

        }
        System.out.println("¡Adios!");
    }
    
}
